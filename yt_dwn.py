#!/usr/bin/env python3
import argparse
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from enum import Enum
import logging
from logging.handlers import RotatingFileHandler
import os
import re
import subprocess
import sys


class MediaType(Enum):
    UNKNOWN = 0
    AUDIO = 1
    VIDEO = 2
    BOTH = 3


class Format():
    def __init__(self, id: str, extension: str, resolution=None,
                 resolution_p=None, bitrate=None, media_type=MediaType.UNKNOWN,
                 additional_info=None):
        self.id = id
        self.extension = extension
        self.resolution = resolution
        self.resolution_p = resolution_p
        self.bitrate = bitrate
        self.media_type = media_type
        self.additional_info = additional_info

    def __str__(self) -> str:
        media_type = "Unknown"
        if self.media_type == MediaType.AUDIO:
            media_type = "Audio"
        elif self.media_type == MediaType.VIDEO:
            media_type = "Video"
        elif self.media_type == MediaType.BOTH:
            media_type = "Both audio and video"
        return f"extension: {self.extension}; resolution: {self.resolution}; " \
               f"bitrate: {self.bitrate}; media_type: {media_type}; " \
               f"additional_info: {self.additional_info}"


def get_audio_codecs():
    codecs = ["best", "aac", "flac", "mp3", "m4a", "opus", "vorbis", "wav"]
    return [{"label": c, "value": c} for c in codecs]


# ----- youtube-dl ---------------------------------------------------------------------------------
def check_youtube_url(url: str):
    """Checks if the URL is a valid youtube URL."""
    try:
        if url:
            match = re.match(r"(http://|https://)*(www.youtube.com/.*)", url)
            if match.group(1) and match.group(2):
                return True
        return False
    except Exception as e:
        logger.error(f"(check_youtube_url) {e}")


def get_formats(url: str) -> dict:
    try:
        process = subprocess.Popen(['youtube-dl', '-F', url], stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        out, err = process.communicate()

        formats = {}
        if out and len(out) > 10:
            tmp = out.decode("utf-8").split("\n")[3:]
            for line in tmp:
                try:
                    match = re.match(
                        r"^(\d+)\s+(.*)\s+(audio only|\d+x\d+)\s+(tiny|\d+p)\s+(\d+[a-z])\s+,\s+(.*)$",
                        line)
                    media_type = MediaType.BOTH
                    if "audio only" in line:
                        media_type = MediaType.AUDIO
                    if "video only" in line:
                        media_type = MediaType.VIDEO
                    formats[match.group(1).strip()] = Format(match.group(1).strip(),
                                                             match.group(2).strip(),
                                                             match.group(3).strip(),
                                                             match.group(4).strip(),
                                                             match.group(5).strip(),
                                                             media_type,
                                                             match.group(6).strip())
                except Exception as e:
                    logger.error(f"(get_formats) Error parsing format line: {line}; {e}")
        # DEBUG
        # for format in formats:
        #     print(str(format))
        return formats
    except Exception as e:
        logger.error(f"(get_formats) {e}")


def download(cmd: str):
    try:
        logger.info(cmd)
        if not os.path.isdir(output_dir):
            logger.info(f"(download) Creating directory: {output_dir}")
            os.mkdir(output_dir)

        logger.info(f"(download) cmd: {cmd}")
        result = os.system(cmd)
        if result != 0:
            logger.warning(f"(download) Download failed! {cmd}")
        return result
    except Exception as e:
        logger.error(f"(download) {e}")


# ----- ui -----------------------------------------------------------------------------------------
def get_quality_options():
    """Basic quality options."""
    quality_options = [
        "Default",
        "Best Audio & Video",
        "Best Audio only",
        "Best Video only",
        "720p Video & Audio",
        "1080p Video & Audio",
        "Advanced"
    ]
    return [{"label": f"{o}", "value": f"{o.replace(' &', '').replace(' ', '_').lower()}"}
            for o in quality_options]


def get_advanced_audio(formats: dict):
    return [{"label": f"{str(f)}", "value": f.id}
            for f in formats.values() if f.media_type == MediaType.AUDIO]


def get_advanced_video(formats: dict):
    return [{"label": f"{str(f)}", "value": f.id}
            for f in formats.values() if f.media_type != MediaType.AUDIO]


# init UI
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H1('Youtube downloader'),

    dcc.Input(type='url', placeholder="Enter your youtube link...", id="ui_url", style=dict(
        width='60%',
    )),
    dcc.Dropdown(
        options=get_quality_options(),
        value=get_quality_options()[0]["value"],
        id="ui_quality_options",

        style=dict(
            width='40%',
            display='inline-block',
            verticalAlign="middle",
        )
    ),
    html.Label(children=""),

    html.Div(id="ui_audio_codecs", style={'display': 'none'}, children=[
        html.Label("Audio codecs: "),
        dcc.Dropdown(
            id="ui_audio_codecs_dd",
            options=get_audio_codecs(),

            style=dict(
                width='100%',
                display='inline-block',
                verticalAlign="middle",
            )
        )
    ]),
    html.Div(id="ui_audio_options", style={'display': 'none'}, children=[
        html.Label("Audio: "),
        dcc.Dropdown(
            id="ui_audio_options_dd",

            style=dict(
                width='100%',
                display='inline-block',
                verticalAlign="middle",
            )
        )
    ]),
    html.Div(id="ui_video_options", style={'display': 'none'}, children=[
        html.Label("Video: "),
        dcc.Dropdown(
            id="ui_video_options_dd",

            style=dict(
                width='100%',
                display='inline-block',
                verticalAlign="middle",
            )
        )
    ]),
    html.Button('Download', n_clicks=0, id="ui_btn_download",
                style={'background': 'red', 'color': 'white', 'horizontalAlign': 'middle'}),
    html.Div(id="ui_data_store", style={'display': 'none'}, children=""),
    html.Label("Status: Ready", id="status_ui_lbl"),

    dcc.Loading(
        id="loading",
        type="default",
        children=html.Div(id="loading-output-1")
    )
])


# ----- callbacks ----------------------------------------------------------------------------------

# -> advanced options -> id(s)


@app.callback([Output('ui_audio_options', 'style'),
               Output('ui_video_options', 'style'),
               Output('ui_audio_options_dd', 'value'),
               Output('ui_video_options_dd', 'value')],
              # Output('ui_audio_options_dd', 'options'),
              # Output('ui_video_options_dd', 'options')],
              [Input('ui_quality_options', 'value'),
               Input('ui_url', 'value')])
def on_advanced_options(option: str, url: str):
    """Show/hide advanced options UI."""
    invisible = {'display': 'none'}
    visible = {}

    if option == "advanced" and check_youtube_url(url):
        return visible, visible, None, None
    else:
        return invisible, invisible, None, None


@app.callback([Output('ui_audio_options_dd', 'options'),
               Output('ui_video_options_dd', 'options')],
              [Input('ui_audio_options', 'style'),
               Input('ui_video_options', 'style')],
              [State('ui_url', 'value')])
def on_advanced_option_visible(style_audio: dict, style_video: dict, url: str):
    """Populates audio and video advanced options."""
    invisible = {'display': 'none'}
    if style_audio != invisible and style_video != invisible and check_youtube_url(url):
        formats = get_formats(url)
        return get_advanced_audio(formats), get_advanced_video(formats)
    else:
        return [], []


@app.callback(Output('ui_audio_codecs', 'style'),
              [Input('ui_quality_options', 'value'),
               Input('ui_audio_options_dd', 'value'),
               Input('ui_video_options_dd', 'value')])
def on_audio_codecs(option: str, audio_options_dd_value: str, ui_video_options_dd_value: str):
    """Show/hide audio codecs UI."""
    invisible = {'display': 'none'}
    visible = {}

    if option == "best_audio_only" or (audio_options_dd_value and not ui_video_options_dd_value):
        return visible
    else:
        return invisible


@app.callback([Output('status_ui_lbl', 'children'),
               Output('loading', 'children')],
              [Input('ui_btn_download', 'n_clicks')],
              [State('ui_quality_options', 'value'),
               State('ui_url', 'value'),
               State('ui_audio_codecs_dd', 'value'),
               State('ui_audio_options_dd', 'value'),
               State('ui_video_options_dd', 'value')])
def on_button_click(download_click: int, option: str, url: str, audio_codec: str,
                    audio_option: str, video_option: str):
    if check_youtube_url(url):
        formats = get_formats(url)

        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        base_cmd = "youtube-dl {} -o " + f"'{output_dir}%(title)s.%(ext)s' " + "{}"
        cmd = ""

        if option != "advanced":
            if option == "default":
                pass  # default is without any special arguments
            elif option == "best_audio_video":
                cmd = "-f bestvideo+bestaudio/best"
            if option == "best_audio_only":
                if audio_codec:
                    cmd = f"-x -f bestaudio --audio-format {audio_codec}"
                else:
                    cmd = "-x -f bestaudio"
            elif option == "best_video_only":
                cmd = "-f bestvideo"
            elif option == "720p_video_audio":
                tmp = "bestvideo[height<=720]"
                for f in formats.values():
                    if f.resolution_p == "720p" and f.extension == "mp4":
                        tmp = f.id  # no break after this, so we can find the highest bitrate
                cmd = f"-f {tmp}+bestaudio"
            elif option == "1080p_video_audio":
                tmp = "bestvideo[height<=1080]"
                for f in formats.values():
                    if f.resolution_p == "1080p" and f.extension == "mp4":
                        tmp = f.id  # no break after this, so we can find the highest bitrate
                cmd = f"-f {tmp}+bestaudio"
        else:
            if audio_option:
                if video_option:
                    cmd = f"-f {video_option}+{audio_option}"
                else:
                    if audio_codec:
                        cmd = f"-x -f {audio_option} --audio-format {audio_codec}"
                    else:
                        cmd = f"-x -f {audio_option}"
            else:
                if video_option:
                    cmd = f"-f {video_option}"
        if download(base_cmd.format(cmd, url)) == 0:
            return "Status: Downloaded", None
        else:
            return "Status: Failed", None
    return "Status: Ready", None

# @app.callback(Output('loading', 'children'),
#               [Input('ui_btn_download', 'n_clicks')])
# def on_button_click2(n_clicks):
#     if n_clicks:
#         return None

def init_logger(name: str, level="info", path=".", print_stdout=False):
    logger = logging.getLogger(name)
    if level == 'error':
        logger.setLevel(logging.ERROR)
    elif level == 'warn':
        logger.setLevel(logging.WARN)
    elif level == 'info':
        logger.setLevel(logging.INFO)
    elif level == 'debug':
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    path = f"{path}/{name}.log"

    handler = RotatingFileHandler(path, maxBytes=5 * 1024 * 1024,
                                  backupCount=4,
                                  encoding='utf-8', mode='w')
    handler.setFormatter(
        logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)
    # commented due to access to the CLI
    if print_stdout:
        handler_stream = logging.StreamHandler(sys.stdout)
        handler_stream.setFormatter(
            logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
        logger.addHandler(handler_stream)
    return logger


def parse_args():
    try:
        parser = argparse.ArgumentParser(description='Process some integers.')
        parser.add_argument('-o', '--output-dir', help='Output directory.', required=True)
        parser.add_argument('-l', '--log', help='Log level.',
                            choices=['error', 'warn', 'info', 'debug'], default='info')

        args = parser.parse_args()
        # DEBUG
        # args = parser.parse_args(["-o", "/tmp/youtube"])
        print(args)

        return args
    except Exception as e:
        print(f"[ERROR] (parse_args) {e}")
        sys.exit(1)


if __name__ == "__main__":
    args = parse_args()
    logger = init_logger("yt_dwn", level=args.log)

    output_dir = args.output_dir
    if output_dir[-1] != "/":
        output_dir += "/"

    app.run_server(host="0.0.0.0", port=8020, debug=True)
